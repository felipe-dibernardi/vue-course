import Portfolio from './components/portfolio/Portfolio.vue'
import StockList from './components/stock/StockList.vue';
import Home from './components/Home.vue';

export const routes = [
  { path: '', component: Home, name: 'home' },
  { path: '/portfolio', component: Portfolio, name: 'portfolio' },
  { path: '/stocks', component: StockList, name: 'stocks' }
]
