const state = {
  stocks: []
};

const mutations = {
  setStocks(state, payload) {
    state.stocks = payload;
  },
  randomStocks(state) {
    state.stocks.forEach(stock => {
      stock.value = Math.floor(Math.random() * stock.value + stock.value/ 2);
    });
  }
}

const actions = {
  buyStocks: (context, payload) => {
    context.commit('buyStock', payload);
  },
  initStocks: (context) => {
    context.commit('setStocks', context.state.stocks.length === 0 ? [
    { name: 'BMW', value: 100 },
    { name: 'Apple', value: 200 },
    { name: 'Google', value: 175 },
    { name: 'Twitter', value: 50 }
    ] : context.state.stocks);
  },
  randomizeStocks: (context) => {
    context.commit('randomStocks');
  }
}

const getters = {
  stocks: state => {
    return state.stocks;
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
