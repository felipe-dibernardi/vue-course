const state = {
  funds: 10000,
  stocks: []
};

const mutations = {
  setPortfolio(state, payload) {
    state.stocks = payload;
  },
  setFunds(state, funds) {
    state.funds = funds;
  },
  buyStock(state, {name, quantity, value}) {
    let portItem = state.stocks.find(element => element.name == name);
    if (portItem) {
      portItem.quantity += quantity;
    } else {
      state.stocks.push({
        name: name,
        quantity: quantity
      });
    }
    state.funds -= quantity * value;
  },
  sellStock(state, {name, quantity, value}) {
    let portItem = state.stocks.find(element => element.name == name);
    portItem.quantity -= quantity;
    if (portItem.quantity === 0) {
      state.stocks.splice(state.stocks.indexOf(portItem), 1);
    }
    state.funds += quantity * value;
  }
}

const actions = {
  sellStocks(context, payload) {
    context.commit('sellStock', payload)
  }
}

const getters = {
  stockPortfolio (state, getters) {
    return state.stocks.map(stock => {
      let record = getters.stocks.find(element => element.name == stock.name);
      return {
        name: stock.name,
        quantity: stock.quantity,
        value: record.value
      }
    })
  },
  funds (state) {
    return state.funds;
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
