import Vue from 'vue';

export const loadData = (context) => {
    Vue.http.get('data.json')
    .then(response => response.json())
    .then(result => {
        console.log('Here')
        if (result) {
            context.commit('setStocks', result.stocks);
            context.commit('setFunds', result.funds);
            context.commit('setPortfolio', result.stockPortfolio ? result.stockPortfolio : []);
        }
        
    })
}