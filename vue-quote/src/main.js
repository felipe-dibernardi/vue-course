import Vue from 'vue'
import App from './App.vue'

export const eventBus = new Vue({
  data: {
    totalQuotes: 0
  },
  methods: {
    addQuote(quote) {
      this.totalQuotes++;
      this.$emit('quoteAdded', quote);
    },
    removeQuote() {
      this.totalQuotes--;
      this.$emit('quoteRemoved');
    }
  }
});

new Vue({
  el: '#app',
  render: h => h(App)
})
