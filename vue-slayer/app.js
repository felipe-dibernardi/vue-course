new Vue({
    el: '#app',
    data: {
        battling: false,
        healthbar: {
            backgroundColor: 'green', 
            margin: 0, 
            color: 'white'  
        },
        winner: '',
        logs: [],
        player: {
            health: 100,
            baseAttack: 3,
            skillAttack: 5,
            healPower: 3,
            attack: function() {
                return Math.round(Math.random() * 10) + this.baseAttack;
            },
            specialAttack: function() {
                return Math.round(Math.random() * 10) + this.skillAttack;
            },
            heal: function() {
                var health = Math.round(Math.random() * 10) + this.healPower;
                if (this.health + health > 100) {
                    this.health = 100;
                } else {
                    this.health += health;
                }
                return health;
            },
            receiveDamage: function(damage) {
                this.health = this.health - damage;
            }
        },
        monster: {
            health: 100,
            baseAttack: 4,
            attack: function() {
                return Math.round(Math.random() * 10) + this.baseAttack;
            },
            receiveDamage: function(damage) {
                this.health = this.health - damage;
            }
        }
    },
    methods: {
        heal: function() {
            var heal = this.player.heal();
            this.logs.push({user: 'player', action: 'Player used heal. Player recovered ' + heal + ' health.'});
            var damage = this.monster.attack();
            this.logs.push({user: 'monster', action: 'Monster attacked. It did ' + damage + ' damage to Player.'});
            this.player.receiveDamage(damage);
            this.checkBattleEnd();
        },
        attack: function() {
            var playerDmg = this.player.attack();
            this.monster.receiveDamage(playerDmg);
            this.logs.push({user: 'player', action: 'Player attacked. Player did ' + playerDmg + ' damage to the monster.'});
            this.checkBattleEnd();
            if (this.battling) {
                var monsterDmg = this.monster.attack();
                this.player.receiveDamage(monsterDmg);
                this.logs.push({user: 'monster', action: 'Monster attacked. It did ' + monsterDmg + ' damage to Player.'});
                this.checkBattleEnd();
            }
            
        },
        specialAttack: function() {
            var playerDmg = this.player.specialAttack();
            this.monster.receiveDamage(playerDmg);
            this.logs.push({user: 'player', action: 'Player used special attack. Player did ' + playerDmg + ' damage to the monster.'});
            this.checkBattleEnd();
            if (this.battling) {
                var monsterDmg = this.monster.attack();
                this.player.receiveDamage(monsterDmg);
                this.logs.push({user: 'monster', action: 'Monster attacked. It did ' + monsterDmg + ' damage to Player.'});
                this.checkBattleEnd();    
            }
        },
        giveUp: function() {
            this.battling = false;
            this.winner = 'Monster';
            this.logs.push({user: 'player', action: 'Player gave up'});
            this.checkBattleEnd()
        },
        startBattle: function() {
            this.logs= [];
            this.logs.push({user: 'none', action: 'Battle started'});
            this.battling = true;
            this.player.health = 100;
            this.monster.health = 100;
        },
        checkBattleEnd: function() {
            if (this.player.health <= 0) {
                console.log(this.player.health);
                this.winner = 'Monster';
                this.battling = false;
            }
            if (this.monster.health <= 0) {
                this.winner = 'Player';
                this.battling = false;
            }

            if (!this.battling) {
                this.logs.push({user: 'none', action: 'Battle ended'});
                alert(this.winner + ' won the battle.');
            }
        }
    }
});