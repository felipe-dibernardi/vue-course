import axios from 'axios'

const instance = axios.create({
    baseURL: 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/'
})

// axios.defaults.headers.common['Authorization'] = 'Basic';

export default instance;