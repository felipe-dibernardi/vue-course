import Vue from 'vue'
import Vuex from 'vuex'
import axios from './axios-auth';
import globalAxios from 'axios';
import router from './router'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    idToken: null,
    userId: null,
    user: null
  },
  mutations: {
    authUser(state, userData) {
      state.idToken = userData.token;
      state.userId = userData.userId;
    },
    storeUser (state, user) {
      state.user = user
    },
    clearAuthData(state) {
      state.idToken = null;
      state.userId = null;
      state.user = null;
    } 
  },
  actions: {
    setLogoutTimer(context, expirationTime) {
      setTimeout(() => {
        context.dispatch('logout');
      }, expirationTime * 1000);
    },
    signup(context, authData) {
      axios.post('/signupNewUser?key=AIzaSyAEjL4Hm-ArqdExNDvzmiVUFXnxobP-T3k', {
        email: authData.email,
        password: authData.password,
        returnSecureToken: true
      })
      .then(response => {
        console.log(response);
        context.commit('authUser', {
          token: response.data.idToken,
          userId: response.data.localId
        })
        let now = new Date();
        let expirationDate = new Date(now.getTime() + response.data.expiresIn * 1000);
        localStorage.setItem('token', response.data.idToken);
        localStorage.setItem('userId', response.data.localId);
        localStorage.setItem('expirationDate', expirationDate);
        context.dispatch('storeUser', authData);
        context.dispatch('setLogoutTimer', response.data.expiresIn);
      })
      .catch(error => console.log(error));
    },
    login(context, authData) {
      axios.post('/verifyPassword?key=AIzaSyAEjL4Hm-ArqdExNDvzmiVUFXnxobP-T3k', {
        email: authData.email,
        password: authData.password,
        returnSecureToken: true
      })
      .then(response => {
        console.log(response);
        context.commit('authUser', {
          token: response.data.idToken,
          userId: response.data.localId
        })
        let now = new Date();
        let expirationDate = new Date(now.getTime() + response.data.expiresIn * 1000);
        localStorage.setItem('token', response.data.idToken);
        localStorage.setItem('userId', response.data.localId);
        localStorage.setItem('expirationDate', expirationDate);
        context.dispatch('setLogoutTimer', response.data.expiresIn);
      })
      .catch(error => console.log(error));
    },
    tryAutoLogin(context) {
      let token = localStorage.getItem('token');
      if (!token) {
        return;
      }
      let expirationDate = localStorage.getItem('expirationDate');
      if (expirationDate <= new Date()) {
        return;
      }
      let userId = localStorage.getItem('userId');
      context.commit('authUser', {token: token, userId: userId})
    },
    logout(context) {
      context.commit('clearAuthData');
      router.replace('/signin');
      localStorage.removeItem('expirationDate');
      localStorage.removeItem('token');
      localStorage.removeItem('userId');
    },
    storeUser ({commit, state}, userData) {
      if (!state.idToken) {
        return
      }
      globalAxios.post('/users.json' + '?auth=' + state.idToken, userData)
        .then(res => console.log(res))
        .catch(error => console.log(error))
    },
    fetchUser ({commit, state}) {
      if (!state.idToken) {
        return
      }
      globalAxios.get('/users.json' + '?auth=' + state.idToken)
        .then(res => {
          console.log(res)
          const data = res.data
          const users = []
          for (let key in data) {
            const user = data[key]
            user.id = key
            users.push(user)
          }
          console.log(users)
          commit('storeUser', users[0])
        })
        .catch(error => console.log(error))
    }
  },
  getters: {
    user (state) {
      return state.user
    },
    isAuthenticated(state) {
      return state.idToken !== null;
    }
  }
})