export const computed = {
    computed: {
        reverse() {
            return this.text.split("").reverse().join("");
        },
        countLetters() {
            return this.text + ' (' + this.text.length + ')';
        }
    }
}